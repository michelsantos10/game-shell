package com.mastertech.itau.game.shell.application.command;

import com.mastertech.itau.game.shell.domain.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.jms.JMSException;

@ShellComponent
public class GameCommand {
    public static Logger logger = (Logger) LoggerFactory.getLogger(GameCommand.class);

    @Value("${com.mastertech.itau.game.shell.application.command.start-game-queue}")
    public String startGameQueue;

    @Value("${com.mastertech.itau.game.shell.application.command.ranking-queue}")
    public String rankingQuestionsQueue;

    @Autowired
    public GameService gameService;

    private void writeCurretQuestion() {

        Integer currentQuestion = this.gameService.getCurrentQuestionNumber() + 1;
        Integer totalQuestion = this.gameService.getAllQuestions().size();
        System.out.println("Pergunta " + currentQuestion.toString() + " de " + totalQuestion.toString());
        System.out.println("[" + this.gameService.getCurrentAnswer().get("category") + "] " + this.gameService.getCurrentAnswer().get("title"));
        Integer i = 0;
        while (true) {
            i++;
            if (!gameService.getCurrentAnswer().containsKey("option" + i)) {
                break;
            }
            System.out.println(i.toString() + ") " + this.gameService.getCurrentAnswer().get("option" + i));
        }
    }

    private void writeRanking() {
        System.out.println("Fim do jogo!");
        System.out.println("Total de acertos: " + this.gameService.getCorrectAnswerCount());
        System.out.println("Total de erros: " + this.gameService.getIncorrectAnswerCount());
    }

    @ShellMethod(key = "start-game", value = "Inicializa o jogo.")
    public void startGame(String playerName, @ShellOption(defaultValue = "conhecimento-geral") String gameName) throws JMSException {
        this.gameService.startNewGame(playerName, gameName, startGameQueue);
        //this.writeCurretQuestion();
    }

    @ShellMethod(key = "answer", value = "Responde uma questão")
    public void answerCurrentQuestion(Short option) throws JMSException {
        if (this.gameService.getCurrentAnswer().get("answer") != null) {
            if (option == Short.parseShort(this.gameService.getCurrentAnswer().get("answer"))) {
                this.gameService.setCorrectAnswerCount((short) (this.gameService.getCorrectAnswerCount() + 1));
            } else {
                this.gameService.setIncorrectAnswerCount((short) (this.gameService.getIncorrectAnswerCount() + 1));
            }
        } else {
            System.out.println("Questão " + gameService.getCurrentQuestionNumber() + "descartada. Não existe resposta correta cadastrada.");
        }
        this.gameService.goToNextQuestion();
        if (!this.gameService.isFinished()) {
            this.writeCurretQuestion();
        } else {
            this.writeRanking();
            this.gameService.publishRanking(this.rankingQuestionsQueue);
        }
    }
}
