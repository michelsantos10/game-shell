package com.mastertech.itau.game.shell.domain.service;

import com.mastertech.itau.game.shell.domain.entity.GameState;
import com.mastertech.itau.game.shell.domain.entity.Question;
import com.mastertech.itau.game.shell.domain.repository.QuestionRepository;
import com.mastertech.itau.game.shell.domain.repository.RankingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import java.util.List;
import java.util.Map;

@Service
public class GameService {
//    public static Logger logger = (Logger) LoggerFactory.getLogger(GameService.class);

    @Autowired
    public QuestionRepository questionRepository;

    @Autowired
    public RankingRepository rankingRepository;

    @Autowired
    public GameStartService gameStartService;

    private GameState state;

    private String idGame;

    @Autowired
    public GameService() {
        this.state = new GameState();
    }

    public void startNewGame(String playerName, String gameName, String queueAllQuestions) throws JMSException {
        this.idGame = gameStartService.startGame(playerName, gameName, queueAllQuestions);
    }

    public boolean isFinished() {
        return this.state.getCurrentQuestionNumber() >= state.getQuestions().size();
    }

    public List<Question> getAllQuestions() {
        return this.state.getQuestions();
    }

    public Short getCurrentQuestionNumber() {
        return this.state.getCurrentQuestionNumber();
    }

    public Map<String, String> getCurrentAnswer() {
        return this.state.getCurrentAnswers();
    }

    public Short getCorrectAnswerCount() {
        return this.state.getCorrectAnswerCount();
    }

    public void setCorrectAnswerCount(Short correctAnswerCount) {
        this.state.setCorrectAnswerCount(correctAnswerCount);
    }

    public Short getIncorrectAnswerCount() {
        return this.state.getIncorrectAnswerCount();
    }

    public void setIncorrectAnswerCount(Short incorrectAnswerCount) {
        this.state.setIncorrectAnswerCount(incorrectAnswerCount);
    }

    public void goToNextQuestion() {
        if (!this.isFinished()) {
            this.state.setCurrentQuestionNumber((short) (this.state.getCurrentQuestionNumber() + 1));
        }
    }

    public void publishRanking(String queueName) throws JMSException {
        this.rankingRepository.putRanking(this.state, queueName);
    }
}
