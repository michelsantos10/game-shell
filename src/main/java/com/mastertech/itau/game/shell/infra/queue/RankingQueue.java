package com.mastertech.itau.game.shell.infra.queue;

import com.mastertech.itau.game.shell.domain.entity.GameState;
import com.mastertech.itau.game.shell.domain.repository.RankingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import java.util.Random;

@Component
public class RankingQueue implements RankingRepository {
    private static Random randomGenerator = new Random();

    @Autowired
    private JmsTemplate jmsTemplate;

    public void putRanking(GameState game, String queueName) throws JMSException {
        jmsTemplate.setReceiveTimeout(10000);
        jmsTemplate.send(queueName, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setString("playerName", game.getPlayerName());
                message.setString("gameId", game.getGameId());
                message.setString("hits", game.getCorrectAnswerCount().toString());
                message.setString("misses", game.getIncorrectAnswerCount().toString());
                message.setString("total", ((Integer) (game.getQuestions().size() + 1)).toString());
                Long randomMessageID = randomGenerator.nextLong() >>> 1;
                message.setJMSMessageID("ID:" + randomMessageID);
                return message;
            }
        });
    }
}
