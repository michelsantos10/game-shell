package com.mastertech.itau.game.shell.infra.queue;

import com.mastertech.itau.game.shell.domain.entity.Question;
import com.mastertech.itau.game.shell.domain.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class QuestionQueue implements QuestionRepository {
    @Autowired
    private JmsTemplate jmsTemplate;

    private ObjectMessage requestQuestions(String gameName, String queueName) throws JMSException {
        jmsTemplate.setReceiveTimeout(10000);
        Message message = jmsTemplate.sendAndReceive(queueName, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setShort("quantity", (short) 9);
                message.setString("random", "yes");
                return message;
            }
        });
        return ((ObjectMessage) message);
    }

    @Override
    public List<Question> getAllQuestions(String gameName, String queueAllQuestions) throws JMSException {
        List<Map<String, String>> response = ((List<Map<String, String>>) this.requestQuestions(gameName, queueAllQuestions).getObject());
        List<Question> questions = new ArrayList<Question>();
        for (Map<String, String> questionMap : response) {
            Question q = new Question();
            for (String key : questionMap.keySet()) {
                q.put(key, questionMap.get(key));
            }
            questions.add(q);
        }
        return questions;
    }
}
