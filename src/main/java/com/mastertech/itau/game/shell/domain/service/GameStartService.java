package com.mastertech.itau.game.shell.domain.service;

import javax.jms.JMSException;

public interface GameStartService {
    String startGame(String userName, String gameName, String startGameQueue) throws JMSException;
}
