package com.mastertech.itau.game.shell.infra.queue;

import com.mastertech.itau.game.shell.domain.service.GameStartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.util.HashMap;

@Component
public class GameQueue implements GameStartService {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Override
    public String startGame(String playerName, String gameName, String startGameQueue) throws JMSException {
//        jmsTemplate.setReceiveTimeout(100000);
        Message message = jmsTemplate.sendAndReceive(startGameQueue, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                MapMessage message = session.createMapMessage();
                message.setString("playerName", playerName);
                message.setString("gameName", gameName);
                return message;
            }
        });
        HashMap<String, String> m = (HashMap<String, String>) ((ObjectMessage) message).getObject();

        return m.get("gameId");
    }
}
