package com.mastertech.itau.game.shell.domain.repository;

import com.mastertech.itau.game.shell.domain.entity.Question;

import javax.jms.JMSException;
import java.util.List;

public interface QuestionRepository {
    List<Question> getAllQuestions(String gameName, String queueAllQuestions) throws JMSException;
}
