package com.mastertech.itau.game.shell.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
@ComponentScan("com.mastertech.itau.game.shell.*")
public class GameShellApplication {
    public static void main(String[] args) {

        SpringApplication.run(GameShellApplication.class, args);
    }
}
