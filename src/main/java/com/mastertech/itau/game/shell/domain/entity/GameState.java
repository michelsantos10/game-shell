package com.mastertech.itau.game.shell.domain.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class GameState {
    public static Logger logger = (Logger) LoggerFactory.getLogger(GameState.class);

    private List<Question> questions;
    private String playerName;
    private String gameId;
    private boolean started;
    private Short currentQuestionNumber;
    private Short correctAnswerCount;
    private Short incorrectAnswerCount;

    public GameState() {
        this.started = false;
        this.currentQuestionNumber = -1;
        logger.debug("constructor -> " + this.toString());
        this.incorrectAnswerCount = 0;
        this.correctAnswerCount = 0;
        this.questions = null;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public Short getCorrectAnswerCount() {
        return correctAnswerCount;
    }

    public void setCorrectAnswerCount(Short correctAnswerCount) {
        this.correctAnswerCount = correctAnswerCount;
    }

    public Short getIncorrectAnswerCount() {
        return incorrectAnswerCount;
    }

    public void setIncorrectAnswerCount(Short incorrectAnswerCount) {
        this.incorrectAnswerCount = incorrectAnswerCount;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Short getCurrentQuestionNumber() {
        logger.debug("getCurrentQuestionNumber -> " + this.toString());
        return this.currentQuestionNumber;
    }

    public void setCurrentQuestionNumber(Short currentQuestionNumber) {
        this.currentQuestionNumber = currentQuestionNumber;
    }

    public Map<String, String> getCurrentAnswers() {
        return this.questions.get(this.getCurrentQuestionNumber());
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
